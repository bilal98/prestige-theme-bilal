window.$ = window.jQuery = $;

require('./vendors/lazy-lod');
require('./vendors/slick-slider');
require('./vendors/jquery-3.6.0');
require('./vendors/links');
require('./sections/slider-first');
require('./sections/weekly-selection');
require('./sections/new-collection');

require('../scss/app.scss');




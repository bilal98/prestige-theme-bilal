// Slick Slide

class slickSlider {
  constructor(dots, arrows, autoplay, autoplaySpeed) {
    this.dots1 = dots;
    this.arrows1 = arrows;
    this.autoplay1 = autoplay;
    this.autoplaySpeed = autoplaySpeed;

    /* Call init method here like this */
    this.init();
  }

  /* Make 1 Init method like this */
  init = () => {
    $(".img-area").slick({
      dots: this.dots1,
      arrows: this.arrows1,
      autoplay: this.autoplay1,
      // autoplay : this.autoplay1,
      autoplaySpeed: this.autoplaySpeed,
    });
  };
}

$(document).ready(function () {
  /* Call the class contructor method like this by initializing the class */
  new slickSlider(true, false, true, 2000);
});
